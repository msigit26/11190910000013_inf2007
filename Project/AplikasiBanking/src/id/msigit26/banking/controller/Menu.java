package id.msigit26.banking.controller;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class Menu {

    private int noMenu;
    private int pilih;
    Scanner in = new Scanner(System.in);

    public void getMenuAwal() {
        System.out.println("=================================================");
        System.out.println("Selamat Datang di Git's Banking");
        System.out.println("=================================================");
        System.out.println("DAFTAR MENU");
        System.out.println("1. Tambah Saldo");
        System.out.println("2. Ambil Saldo");
        System.out.println("3. Transfer");
        System.out.println("4. Info");
        System.out.println("5. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3/4/5): ");
        noMenu = in.nextInt();

        if (noMenu <= 0 || noMenu > 5) {
            System.out.println("Input Salah!!");
            System.out.println("Apakah anda ingin input kembali? ");
            System.out.print("(1. ya\t2. tidak): ");
            pilih = in.nextInt();
            switch (pilih) {
                case 1:
                    Pilih();
                    break;
                case 2:
                    System.out.println("Merci... Sampai Jumpa :)");
                    System.exit(0);
                    break;
            }
        } else {
            setPilihMenu();
        }
    }

    public void Pilih() {
        switch (pilih) {
            case 1:
                getMenuAwal();
                break;
            case 2:
                System.out.println("Sampai jumpa :)");
                break;
        }
    }

    public void setPilihMenu() {
        BankingController bc = new BankingController();
        switch (noMenu) {
            case 1:
                bc.setTambahSaldo();
                break;
            case 2:
                bc.setAmbilSaldo();
                break;
            case 3:
                bc.setTransfer();
                break;
            case 4:
                bc.getDataBanking();
                break;
            case 5:
                System.out.println("Merci... Sampai Jumpa :)");
                System.exit(0);
                break;
        }
    }
}

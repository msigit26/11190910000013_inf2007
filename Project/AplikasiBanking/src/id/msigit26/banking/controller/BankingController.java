package id.msigit26.banking.controller;

import com.google.gson.Gson;
import id.msigit26.banking.model.Banking;
import id.msigit26.banking.model.Info;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author HP
 */
public class BankingController {

    private final String FILE = "C:\\gitlab\\11190910000013_inf2007\\Project\\AplikasiBanking\\banking.json";
    private static BigDecimal totalSaldo;
    private BigDecimal besaran;
    private LocalDateTime waktu;
    private String formatWaktu;
    private DateTimeFormatter dateTimeFormat;
    private Scanner in;
    private String perihal;
    private Banking banking;
    private int Pilih;
    private int pilih;

    public BankingController() {
        in = new Scanner(System.in);
        waktu = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    public void setTambahSaldo() {
        banking = new Banking();
        formatWaktu = waktu.format(dateTimeFormat);

        List<Banking> bankings = getReadBanking(FILE);
        List<Banking> bResults = bankings.stream().collect(Collectors.toList());
        BigDecimal total = bResults.stream()
                .map(Banking::getBesaran)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        totalSaldo = total;
        perihal = "Tambah Saldo";
        System.out.print("masukkan tambahan saldo: Rp. ");
        besaran = in.nextBigDecimal();
        totalSaldo = totalSaldo.add(besaran);
        System.out.println("Tambah Saldo Sebesar Rp. " + besaran + " Berhasil!!\n");
        banking.setFormatWaktu(formatWaktu);
        banking.setPerihal(perihal);
        banking.setBesaran(besaran);
        banking.setTotalSaldo(totalSaldo);
        setWriteBanking(FILE, banking);
        Menu m = new Menu();
        m.getMenuAwal();
    }

    public void setAmbilSaldo() {
        banking = new Banking();
        formatWaktu = waktu.format(dateTimeFormat);

        List<Banking> bankings = getReadBanking(FILE);
        List<Banking> bResults = bankings.stream().collect(Collectors.toList());
        BigDecimal total = bResults.stream()
                .map(Banking::getBesaran)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        totalSaldo = total;
        perihal = "Ambil Saldo";
        System.out.print("masukkan jumlah saldo yang ingin diambil: Rp. ");
        besaran = in.nextBigDecimal();

        if (total.compareTo(besaran) >= 0) {
            System.out.println("Ambil Saldo Sebesar Rp. " + besaran + " Berhasil!!\n");
            besaran = besaran.multiply(new BigDecimal(-1));
            totalSaldo = totalSaldo.add(besaran);
            banking.setFormatWaktu(formatWaktu);
            banking.setPerihal(perihal);
            banking.setBesaran(besaran);
            banking.setTotalSaldo(totalSaldo);
            setWriteBanking(FILE, banking);

            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            System.out.println("Jumlah Saldo Tidak Mencukupi!!");
            pilihAmbilSaldo();
        }
    }

    public void pilihAmbilSaldo() {
        System.out.println("Apakah anda ingin masukkan saldo lagi?");
        System.out.print("(1. ya, 2. tidak): ");
        Pilih = in.nextInt();
        if (Pilih == 1) {
            setAmbilSaldo();
        } else if (Pilih == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            System.out.println("Input Salah!!");
            pilihAmbilSaldo();
        }
    }

    public void setTransfer() {
        banking = new Banking();
        formatWaktu = waktu.format(dateTimeFormat);

        List<Banking> bankings = getReadBanking(FILE);
        List<Banking> bResults = bankings.stream().collect(Collectors.toList());
        BigDecimal total = bResults.stream()
                .map(Banking::getBesaran)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        totalSaldo = total;
        System.out.println("1. Listrik");
        System.out.println("2. BPJS");
        System.out.println("3. Token");
        System.out.println("4. Lainnya");
        System.out.print("Pilih Menu (1/2/3/4): ");
        Pilih = in.nextInt();

        if (Pilih < 1 || Pilih > 4) {
            System.out.println("Input Salah!!");
            System.out.println("Apakah anda ingin input kembali? ");
            System.out.print("(1. ya\t2. tidak): ");
            pilih = in.nextInt();
            switch (pilih) {
                case 1:
                    setTransfer();
                    break;
                case 2:
                    Menu m = new Menu();
                    m.getMenuAwal();
                    break;
            }
        } else {
            switch (Pilih) {
                case 1:
                    perihal = "Listrik";
                    break;
                case 2:
                    perihal = "BPJS";
                    break;
                case 3:
                    perihal = "Token";
                    break;
                case 4:
                    System.out.print("transfer pembayaran: ");
                    perihal = in.next();
                    break;
            }
        }

        perihal = "Bayar " + perihal;
        System.out.print("masukkan jumlah yang ingin ditransfer: Rp. ");
        besaran = in.nextBigDecimal();

        if (total.compareTo(besaran) >= 0) {
            System.out.println("Transfer Sebesar Rp. " + besaran + " Berhasil!!\n");
            besaran = besaran.multiply(new BigDecimal(-1));
            totalSaldo = totalSaldo.add(besaran);
            banking.setFormatWaktu(formatWaktu);
            banking.setPerihal(perihal);
            banking.setBesaran(besaran);
            banking.setTotalSaldo(totalSaldo);
            setWriteBanking(FILE, banking);
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            System.out.println("Jumlah Saldo Tidak Mencukupi!!");
            pilihTransfer();
        }
    }

    public void pilihTransfer() {
        System.out.println("Apakah anda ingin transfer ulang?");
        System.out.print("(1. ya, 2. tidak): ");
        Pilih = in.nextInt();
        if (Pilih == 1) {
            setTransfer();
        } else if (Pilih == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            System.out.println("Input Salah!!");
            pilihTransfer();
        }
    }

    public void setWriteBanking(String file, Banking banking) {
        Gson gson = new Gson();

        List<Banking> bankings = getReadBanking(file);
        bankings.remove(banking);
        bankings.add(banking);

        String json = gson.toJson(bankings);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();

        } catch (IOException ex) {
            Logger.getLogger(BankingController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Banking> getReadBanking(String file) {
        List<Banking> bankings = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);

            while ((line = br.readLine()) != null) {
                Banking[] bs = gson.fromJson(line, Banking[].class
                );
                bankings.addAll(Arrays.asList(bs));
            }
            br.close();
            reader.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(BankingController.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BankingController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return bankings;
    }

    public void getDataBanking() {
        Info info = new Info();
        List<Banking> bankings = getReadBanking(FILE);

        List<Banking> bResults = bankings.stream().collect(Collectors.toList());
        BigDecimal total = bResults.stream()
                .map(Banking::getBesaran)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("\n---------------------------------------------------------------------");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("------------------- \t------------ \t------------- \t-------------");
        System.out.println("\tWaktu \t   \t   Perihal \t   Sejumlah \t    Saldo");
        System.out.println("------------------- \t------------ \t------------- \t-------------");
        bResults.forEach((b) -> {
            System.out.println(b.getFormatWaktu() + "\t" + b.getPerihal() + "  \tRp. " + b.getBesaran() + "\tRp. " + b.getTotalSaldo());
        });
        System.out.println("---------------------------------------------------------------------");
        System.out.println("\t\t\tSaldo Akhir\t\t\tRp. " + total);
        System.out.println("---------------------------------------------------------------------\n");
        System.out.println("Apakah anda ingin Transaksi Kembali?");
        System.out.print("(1. ya, 2. tidak): ");
        Pilih = in.nextInt();
        if (Pilih == 1) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else if (Pilih == 2) {
            System.out.println("Merci... Sampai Jumpa :)");
        } else {
            System.out.println("Input Salah!!");
            getDataBanking();
        }
    }
}

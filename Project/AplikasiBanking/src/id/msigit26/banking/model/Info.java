package id.msigit26.banking.model;

/**
 *
 * @author HP
 */
public class Info {
    String aplikasi ="Aplikasi Banking Sederhana";
    String version ="Versi 1.0.0";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;
    }
}

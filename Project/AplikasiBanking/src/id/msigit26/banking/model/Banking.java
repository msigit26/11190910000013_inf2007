package id.msigit26.banking.model;

import java.math.BigDecimal;

/**
 *
 * @author HP
 */
public class Banking {
    private String formatWaktu;
    private String perihal;
    private BigDecimal besaran;
    private BigDecimal totalSaldo;
    
     public Banking() {
         
    }
    public Banking(String formatWaktu, String perihal, BigDecimal besaran, BigDecimal totalSaldo) {
        this.formatWaktu = formatWaktu;
        this.perihal = perihal;
        this.besaran = besaran;
        this.totalSaldo = totalSaldo;
    }

    public String getFormatWaktu() {
        return formatWaktu;
    }

    public void setFormatWaktu(String formatWaktu) {
        this.formatWaktu = formatWaktu;
    }

    public String getPerihal() {
        return perihal;
    }

    public void setPerihal(String perihal) {
        this.perihal = perihal;
    }

    public BigDecimal getBesaran() {
        return besaran;
    }

    public void setBesaran(BigDecimal besaran) {
        this.besaran = besaran;
    }

    public BigDecimal getTotalSaldo() {
        return totalSaldo;
    }

    public void setTotalSaldo(BigDecimal totalSaldo) {
        this.totalSaldo = totalSaldo;
    }
    
    @Override
    public String toString() {
        return formatWaktu + "\t" + perihal + "  \tRp. " + besaran + "\tRp. " + totalSaldo;
    }
}

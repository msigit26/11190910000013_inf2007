package id.msigit26.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class OutputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("masukkan bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "bilangan: " + bilangan, "hasil input", JOptionPane.INFORMATION_MESSAGE);
    }
}

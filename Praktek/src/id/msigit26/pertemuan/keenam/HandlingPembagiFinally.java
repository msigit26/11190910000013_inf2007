 package id.msigit26.pertemuan.keenam;

/**
 *
 * @author HP
 */
public class HandlingPembagiFinally {

    public static void main(String[] args) {
        try {
            int a = 10;
            int b = 0;
            int c = a / b;

            System.out.println("hasil: " + c);
        } catch (Throwable error) {
            System.out.print("ups, terjadi error: ");
            System.out.println(error.getMessage());
        } finally {
            System.out.println("pasti akan dijalankan");
        }
    }
}

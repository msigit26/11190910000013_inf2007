package id.msigit26.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author HP
 */
public class InputJOptionPaneEx {

    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("masukkan Bilangan: ");

        bilangan = Integer.parseInt(box);

        System.out.println("bilangan: " + bilangan);
    }
}

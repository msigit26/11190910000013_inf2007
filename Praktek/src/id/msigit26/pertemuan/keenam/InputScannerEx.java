package id.msigit26.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class InputScannerEx {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner (System.in);
        System.out.print("masukkan bilangan: ");
        bilangan = in.nextInt();
        
        System.out.println("bilangan: " + bilangan);
    }
}

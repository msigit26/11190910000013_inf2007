package id.msigit26.pertemuan.pertama;

/**
 *
 * @author HP
 */

/* PROGRAM HelloWorld */
/* penjelasan untuk mencetak "Hello world". masukan program ini tidak ada.
 keluarannya adalah tulisan "Hello, World" tercetak di layar */
public class HelloWorld {
    /* deklarasi */
    /* tidak ada */

    /* ALGORITMA */
    public static void main(String[] args) {
        System.out.println("Hello, World");
    }
}
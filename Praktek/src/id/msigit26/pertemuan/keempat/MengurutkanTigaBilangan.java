package id.msigit26.pertemuan.keempat;

/**
 *
 * @author HP
 */
    import java.util.Scanner;

public class MengurutkanTigaBilangan {

    public static void main(String[] args) {
        int a, b, c, terbesar = 0, tengah = 0, terkecil = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("a = ");
        a = in.nextInt();
        System.out.print("b = ");
        b = in.nextInt();
        System.out.print("c = ");
        c = in.nextInt();

        if ((a >= b) && (a >= c)) {
            if (b >= c) {
                terbesar = a;
                tengah = b;
                terkecil = c;
            } else {
                terbesar = a;
                tengah = c;
                terkecil = b;
            }
        }

        if ((b >= a) && (b >= c)) {
            if (a >= c) {
                terbesar = b;
                tengah = a;
                terkecil = c;
            } else {
                terbesar = b;
                tengah = c;
                terkecil = a;
            }
        }
        if ((c >= a) && (c >= b)) {
            if (a >= b) {
                terbesar = c;
                tengah = a;
                terkecil = b;
            } else {
                terbesar = c;
                tengah = b;
                terkecil = a;
            }
        }

        System.out.println("terkecil = " + terkecil + ", tengah = " + tengah + ", terbesar = " + terbesar);
    }
}

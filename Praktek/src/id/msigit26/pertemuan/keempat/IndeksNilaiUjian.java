package id.msigit26.pertemuan.keempat;

/**
 *
 * @author HP
 */
import java.util.Scanner;

public class IndeksNilaiUjian {

    public static void main(String[] args) {
        char indeks;
        int nilai;
        Scanner in = new Scanner(System.in);
        System.out.print("nilai = ");
        nilai = in.nextInt();

        if (nilai >= 80) {
            indeks = 'A';
        } else {
            if ((nilai >= 70) && (nilai < 80)) {
                indeks = 'B';
            } else {
                if ((nilai >= 55) && (nilai < 70)) {
                    indeks = 'C';
                } else {
                    if ((nilai >= 40) && (nilai < 55)) {
                        indeks = 'D';
                    } else {
                        indeks = 'E';
                    }
                }
            }
        }
        System.out.println("indeks : " + indeks);
    }
}

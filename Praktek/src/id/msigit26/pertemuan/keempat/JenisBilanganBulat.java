package id.msigit26.pertemuan.keempat;

/**
 *
 * @author HP
 */
import java.util.Scanner;

public class JenisBilanganBulat {

    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.print("bilangan = ");
        bilangan = in.nextInt();

        if (bilangan > 0) {
            System.out.println("positif");
        } else {
            if (bilangan < 0) {
                System.out.println("negatif");
            } else {
                if (bilangan == 0) {
                    System.out.println("nol");
                }
            }
        }
    }
}

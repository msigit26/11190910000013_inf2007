package id.msigit26.pertemuan.keempat;

/**
 *
 * @author HP
 */
import java.util.Scanner;

public class BilanganKelipatanTiga {

    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.print("bilangan = ");
        bilangan = in.nextInt();

        if (bilangan % 3 == 0) {
            System.out.println("bilangan kelipatan tiga");
        } else {
            System.out.println("bukan kelipatan tiga");
        }
    }
}

package id.msigit26.pertemuan.keempat;

/**
 *
 * @author HP
 */
import java.util.Scanner;

public class DiskonHarga {

    public static void main(String[] args) {
        int totalBelanja;
        float diskon = 0, total;
        Scanner in = new Scanner(System.in);
        System.out.print("total belanja = ");
        totalBelanja = in.nextInt();

        if (totalBelanja > 120000) {
            diskon = totalBelanja * 7 / 100;
            total = totalBelanja - diskon;
        } else {
            total = totalBelanja;
        }
        System.out.println("diskon = Rp. " + diskon);
        System.out.println("harga belanja = Rp. " + total);
    }
}

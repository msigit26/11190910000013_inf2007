package id.msigit26.pertemuan.keempat;

/**
 *
 * @author HP
 */
import java.util.Scanner;

public class HurufVokal {

    public static void main(String[] args) {
        char k;
        Scanner in = new Scanner(System.in);
        System.out.print("masukkan karakter = ");
        k = in.next().charAt(0);

        if ((k == 'a') || (k == 'i') || (k == 'u') || (k == 'e') || (k == 'o')) {
            System.out.println("huruf vokal");
        }
    }
}

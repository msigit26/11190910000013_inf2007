package id.msigit26.pertemuan.keduabelas;

/**
 *
 * @author HP
 */
public class PenyimpananUang extends Tabungan {

    private double tingkatBunga;

    public PenyimpananUang(int saldo, double tingkatBunga) {
        super(saldo);
        this.tingkatBunga = tingkatBunga;
    }
    
    public double cekUang() {
        return tingkatBunga = saldo + (saldo * tingkatBunga);
    }
}

package id.msigit26.pertemuan.keduabelas;

/**
 *
 * @author HP
 */
public class Tabungan {
    protected int saldo;
    
    public Tabungan (int saldo) {
        this.saldo = saldo;
    }
    
    public int ambilUang (int jumlah) {
        saldo = saldo - jumlah;
        return saldo;
    }
}

package id.msigit26.pertemuan.ketiga;

/**
 *
 * @author HP
 */
public class IncDec {

    public static void main(String[] args) {
        int x = 8, y = 13;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("x = " + ++x);
        System.out.println("y = " + y++);
        System.out.println("x = " + x--);
        System.out.println("y = " + --y);
    }
}

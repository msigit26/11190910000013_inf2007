package id.msigit26.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author HP
 */
public class AplikasiKabisat {

    public static void main(String[] args) {
        int tahun;

        Scanner in = new Scanner(System.in);
        System.out.print("tahun: ");
        tahun = in.nextInt();

        Kabisat kabisat = new Kabisat();
        if (kabisat.getHasil(tahun)) {
            System.out.println("tahun kabisat");
        } else {
            System.out.println("bukan tahun kabisat");
        }
    }
}

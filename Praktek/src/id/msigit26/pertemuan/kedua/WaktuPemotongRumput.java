package id.msigit26.pertemuan.kedua;

/**
 *
 * @author HP
 */
import java.util.Scanner;

public class WaktuPemotongRumput {

    public static void main(String[] args) {
        int a, b, c, d;
        float e;
        Scanner masukan = new Scanner(System.in);
        System.out.print("panjang tanah = ");
        a = masukan.nextInt();
        System.out.print("lebar tanah = ");
        b = masukan.nextInt();
        System.out.print("panjang rumah = ");
        c = masukan.nextInt();
        System.out.print("lebar rumah = ");
        d = masukan.nextInt();
        e = (float) ((a * b - c * d) / 2.5);
        System.out.println("waktu yang dibutuhkan " + e);
    }
}
